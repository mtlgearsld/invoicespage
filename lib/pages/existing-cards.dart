import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:invoicesPage/services/payment-service.dart';
import 'package:stripe_payment/stripe_payment.dart';

class ExistingCardsPage extends StatefulWidget {
  @override
  _ExistingCardsPageState createState() => _ExistingCardsPageState();
}

class _ExistingCardsPageState extends State<ExistingCardsPage> {
  //FIXME: you would not have a clients credit card need to push new card to exisiting card data
  List cards = [
    {
      'cardNumber': '4242424242424242',
      'expiryDate': '12/2022',
      'cardHolderName': 'Micheal Jackson',
      'cvvCode': '338',
      'showBackView': false,
    },
    {
      'cardNumber': '378282246310005',
      'expiryDate': '10/2025',
      'cardHolderName': 'Mr. Jaxx',
      'cvvCode': '874',
      'showBackView': false,
    }
  ];

  payViaExistingCard(BuildContext context, card) async {
    var expiryArr = card['expiryDate'].split('/');
    CreditCard stripeCard = CreditCard(
      number: card['cardNumber'],
      expMonth: int.parse(expiryArr[0]),
      expYear: int.parse(expiryArr[1]),
    );
    var response = await StripeService.payViaExistingCard(
        currency: 'cad', amount: '1500', card: stripeCard);
    if (response.success == true) {
      Scaffold.of(context)
          .showSnackBar(
            SnackBar(
              content: Text(response.message),
              duration: Duration(milliseconds: 1200),
            ),
          )
          .closed
          .then((_) => Navigator.pop(context));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chose a card'),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: ListView.builder(
            itemCount: cards.length,
            itemBuilder: (BuildContext context, int index) {
              var card = cards[index];
              return InkWell(
                onTap: () {
                  payViaExistingCard(context, card);
                },
                child: CreditCardWidget(
                  cardNumber: card['cardNumber'],
                  expiryDate: card['expiryDate'],
                  cardHolderName: card['cardHolderName'],
                  cvvCode: card['cvvCode'],
                  showBackView: false,
                ),
              );
            }),
      ),
    );
  }
}
