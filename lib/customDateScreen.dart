import 'package:flutter/material.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class CustomDatePicker extends StatefulWidget {
  @override
  _CustomDatePickerState createState() => _CustomDatePickerState();
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  var rangeDateValue = TextEditingController();

  void _showDate() async {
    print(DateTime(2020).toIso8601String());

    final List<DateTime> picked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: new DateTime.now(),
        initialLastDate: (new DateTime.now()).add(new Duration(days: 7)),
        firstDate: new DateTime(2015),
        lastDate: new DateTime(2021));
    if (picked != null && picked.length == 2) {
      print(picked);
    }

    setState(() {
      rangeDateValue.text = picked.toString();
      // rangeDateValue.text = picked.toString().split(' ')[0];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Date Picker'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Text('Time Period'),
              ),
              ListTile(
                leading: Text('Custom'),
              ),
              ListTile(
                  leading: Text('Time Period :'),
                  trailing: Text(
                    'Set From',
                    style: TextStyle(color: Colors.green),
                  ),
                  onTap: () {
                    _showDate();
                  }),
              ListTile(
                leading: Text('Days chosen:'),
                trailing: Text(
                  '${rangeDateValue.text}', //FIXME: use the split function, text overflows
                  style: TextStyle(color: Colors.green),
                ),
              ),
              SizedBox(height: 100),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Accept'),
                    color: Colors.green,
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  FlatButton(
                    onPressed: () {
                      _showDate();
                    },
                    child: Text('Chnage'),
                    color: Colors.red,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
