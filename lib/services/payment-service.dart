import 'package:flutter/services.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class StripTransactionResponse {
  String message;
  bool success;
  StripTransactionResponse({this.message, this.success});
}

class StripeService {
  //FIXME: should be in a .env file
  static String apiBase = 'https://api.stripe.com/v1';
  static String secret =
      'sk_test_51HbsTjLNT4A5kwgz0Tj3wf9whIR9dhkAianuklMtwx8oa7bkcYTLVK98NHlNLotyy7W9dBh5Wc3IwqaNaxiRRK4z00M2Au2VRr';
  static String paymentApiUrl = '${StripeService.apiBase}/payment_intents';

  static Map<String, String> headers = {
    'Authorization': 'Bearer ${StripeService.secret}',
    'Contebt-Type': 'application/x-www-form-urlencoded'
  };

  static init() {
    StripePayment.setOptions(StripeOptions(
        publishableKey:
            "pk_test_51HbsTjLNT4A5kwgzqiZPhbfGwjcjuOcNTl6isfQbkCNdzWyGQOM0lWLlPFJTyIBgvqMSlwmHK0l0hyNEYGUh5mxC00DDkbY5Rw",
        merchantId: "Test",
        androidPayMode: 'test'));
  }

  static Future<StripTransactionResponse> payViaExistingCard(
      {String amount, String currency, CreditCard card}) async {
    try {
      var paymentMethod = await StripePayment.createPaymentMethod(
          PaymentMethodRequest(
              card: card) // Using exisitng card but (can also use the token)
          );
      var paymentIntent =
          await StripeService.createPaymentIntent(amount, currency);
      var response = await StripePayment.confirmPaymentIntent(
        PaymentIntent(
            clientSecret: paymentIntent['client_secret'],
            paymentMethodId: paymentMethod.id),
      );
      if (response.status == 'succeeded') {
        return StripTransactionResponse(
            message: 'Transaction successful', success: true);
      } else {
        return StripTransactionResponse(
            message: 'Transaction failed', success: false);
      }
    } on PlatformException catch (e) {
      //to show a better erro msg to user
      return StripeService.getPlatformExceptionErrorResult(e);
    } catch (e) {
      return StripTransactionResponse(
          message: 'Transaction failed: ${e.toString()}', success: false);
    }
  }

  static Future<StripTransactionResponse> payWithNewCard(
      {String amount, String currency}) async {
    try {
      var paymentMethod = await StripePayment.paymentRequestWithCardForm(
          CardFormPaymentRequest());
      // print(jsonEncode(paymentMethod)); //encode to see the payment method details
      var paymentIntent =
          await StripeService.createPaymentIntent(amount, currency);
      var response = await StripePayment.confirmPaymentIntent(
        PaymentIntent(
            clientSecret: paymentIntent['client_secret'],
            paymentMethodId: paymentMethod.id),
      );
      if (response.status == 'succeeded') {
        return StripTransactionResponse(
            message: 'Transaction successful', success: true);
      } else {
        return StripTransactionResponse(
            message: 'Transaction failed', success: false);
      }
    } on PlatformException catch (e) {
      //error msg to user
      return StripeService.getPlatformExceptionErrorResult(e);
    } catch (e) {
      return StripTransactionResponse(
          message: 'Transaction failed: ${e.toString()}', success: false);
    }
  }

  static getPlatformExceptionErrorResult(e) {
    String message = 'Something went wrong';
    if (e.code == 'cancelled') {
      message = 'Transaction cancelled';
    }
    return StripTransactionResponse(message: message, success: false);
  }

  static Future<Map<String, dynamic>> createPaymentIntent(
      String amount, String currnecy) async {
    try {
      Map<String, dynamic> body = {
        'amount': amount,
        'currency': currnecy,
        'payment_method_types[]': 'card'
      };
      var response = await http.post(StripeService.paymentApiUrl,
          body: body, headers: StripeService.headers);
      return jsonDecode(response.body);
    } catch (e) {
      print(' err charging user: ${e.toString()}');
      return null;
    }
  }
}
