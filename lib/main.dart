import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:invoicesPage/customDateScreen.dart';
import 'package:invoicesPage/addCardScreen.dart';
import 'secondPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: <String, WidgetBuilder>{
        '/CustomDatePicker': (BuildContext context) => CustomDatePicker()
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _selectedPeriod = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddCardScreen(),
                  ), //nav to prodlist page ProductListWdiget(id)
                ),
                child: Align(
                  alignment: Alignment.topRight,
                  child: Icon(
                    Icons.credit_card,
                    size: 40,
                  ),
                ),
              ),
              GestureDetector(
                // FIXME: make "Pay >" the only text that has navigation
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SecondPage(),
                  ), //nav to prodlist page ProductListWdiget(id)
                ),
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: '\$0.00',
                        style: TextStyle(color: Colors.black, fontSize: 35),
                      ),
                      TextSpan(
                        text: '   ',
                      ),
                      TextSpan(
                        text: 'Pay >',
                        style: TextStyle(color: Colors.blue, fontSize: 30),
                        recognizer: TapGestureRecognizer(), //FIXME:
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                'You\'re All Caught Up!',
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Unpaid Orders',
                        style: TextStyle(color: Colors.red, fontSize: 25),
                      ),
                      Text(
                        'For Grubx Market Suppliers',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                    ],
                  ),
                ),
              ), //statusMessage
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Paid Orders',
                        style: TextStyle(color: Colors.green, fontSize: 25),
                      ),
                      Text(
                        'For Grubx Market Suppliers',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      ),
                    ],
                  ),
                ),
              ),
              Text(
                _selectedPeriod,
                style: TextStyle(fontSize: 20),
              ),
              Expanded(
                child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: GestureDetector(
                    onTap: () => _filterButton(),
                    child: Card(
                      color: Colors.grey.shade200,
                      child: ListTile(
                        leading: Icon(
                          Icons.filter_list,
                          color: Colors.green,
                        ),
                        title: Text(
                          'Filter all transaction by time period',
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),

              //statusMessage
            ],
          ),
        ),
      ),
    );
  }

  void _filterButton() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Text(
                'Time Period',
                style: TextStyle(fontSize: 25),
              ),
            ),
            ListTile(
              leading: Icon(Icons.calendar_view_day),
              title: Text('Last 30 days', style: TextStyle(fontSize: 20)),
              onTap: () => _selectPeriod('30 days'),
            ),
            Divider(
              thickness: 1,
            ),
            ListTile(
              leading: Icon(Icons.calendar_view_day),
              title: Text('Last 60 days', style: TextStyle(fontSize: 20)),
              onTap: () => _selectPeriod('60 days'),
            ),
            Divider(
              thickness: 1,
            ),
            ListTile(
              leading: Icon(Icons.calendar_view_day),
              title: Text('Last 90 days', style: TextStyle(fontSize: 20)),
              onTap: () => _selectPeriod('90 days'),
            ),
            Divider(
              thickness: 1,
            ),
            ListTile(
                leading: Icon(Icons.calendar_view_day),
                title: Text('Custom', style: TextStyle(fontSize: 20)),
                onTap: () => Navigator.pushNamed(context, '/CustomDatePicker')),
          ],
        );
      },
    );
  }

  void _selectPeriod(String period) {
    Navigator.pop(context);
    setState(() {
      _selectedPeriod = period;
    });
  }
}
